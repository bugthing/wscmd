# frozen_string_literal: true

require_relative "lib/wscmd/version"

Gem::Specification.new do |spec|
  spec.name          = "wscmd"
  spec.version       = Wscmd::VERSION
  spec.authors       = ["Benjamin Martin"]
  spec.email         = ["ben.martin@smartpension.co.uk"]

  spec.summary       = "Run a command and have stdout and stderr yielded."
  spec.description   = "Run a command and have stdout and stderr yielded."
  spec.homepage      = "http://www.gitlab.com/bugthing/wscmd"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 3.0.0")

  spec.metadata["allowed_push_host"] = "http://mygemserver.com"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://www.gitlab.com/bugthing/wscmd"
  spec.metadata["changelog_uri"] = "https://www.gitlab.com/bugthing/wscmd/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "file-tail", "~> 1.2.0"
end
