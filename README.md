# Wscmd

This little gem models the concept of running a shell command and redirecting its stdout and stderr to a files which you are tailing in another process.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'wscmd'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install wscmd

## Usage

Shown below is using the gem to run a bash command in 1 process and have each line written to stdout or stderr yielded to a block running in a separate process.

```
Wscmd.stdout(->(line) { puts "Doing something with: #{line} from process: #{$$}" })
     .stderr(->(line) { puts "Doing something with: #{line} from process: #{$$}" })
     .run("bash -c 'echo some stdout; echo some stderr 1>&2'")
```

This gem orginated from the need to do the following from a rails controller

```
class SomeController
  def an_action
    fork do
      Wscmd.stdout(->(line) { ActionCable.server.broadcast('info:log', line) })
           .stderr(->(line) { ActionCable.server.broadcast('info:err', line) })
           .run("./bin/long_running_script")
    end

    render :websocket_page_monitoring_long_running_script
  end
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/wscmd. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/wscmd/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Wscmd project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/bugthing/wscmd/blob/master/CODE_OF_CONDUCT.md).
