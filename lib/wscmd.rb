# frozen_string_literal: true

require_relative "wscmd/version"

require "tempfile"
require "file-tail"
require "open3"
require "timeout"

# High level abstraction over the whole concept
module Wscmd
  class Error < StandardError; end

  module_function

  def stdout(yieldme)
    Command.new.stdout(yieldme)
  end

  def stderr(yieldme)
    Command.new.stderr(yieldme)
  end

  def run(cmd)
    Command.new.run(cmd)
  end

  # Represents the command to run, its stdout, stderr and lock files
  class Command
    attr_reader :cmd, :stdout_block, :stderr_block

    def stdout(yieldme)
      @stdout_block = yieldme
      self
    end

    def stderr(yieldme)
      @stderr_block = yieldme
      self
    end

    def run(cmd)
      @cmd = cmd
      Executor.call(self)
    end

    def stdout_path
      stdout_file.path
    end

    def stderr_path
      stderr_file.path
    end

    def running_path
      @running_path ||= begin
        file = Tempfile.new("running")
        path = file.path
        file.unlink
        path
      end
    end

    private

    def stdout_file
      @stdout_file ||= Tempfile.new("stdout")
    end

    def stderr_file
      @stderr_file ||= Tempfile.new("stderr")
    end
  end

  # Executes the processes
  class Executor
    class << self
      include Process

      def call(command)
        raise Error, "already running! (#{command.running_path})" if File.exist?(command.running_path)

        File.write(command.running_path, "running")

        stdout_broadcast_pid = fork_tail_process(command.stdout_path, command.stdout_block)
        stderr_broadcast_pid = fork_tail_process(command.stderr_path, command.stderr_block)

        cmd_pid = fork_cmd_process(command, stdout_broadcast_pid, stderr_broadcast_pid)

        wait_or_timeout(cmd_pid)
      end

      private

      CMD_TIMEOUT = 600

      def wait_or_timeout(pid)
        Timeout.timeout(CMD_TIMEOUT) do
          waitpid(pid)
        end
        $CHILD_STATUS.exitstatus
      rescue Timeout::Error
        Process.kill("TERM", pid)
        nil
      end

      def fork_tail_process(file, block)
        fork do
          File.open(file) do |f|
            f.extend(File::Tail)
            f.interval = 0
            f.tail { |l| block.call(l) }
          end
        end
      end

      def fork_cmd_process(command, *pids_to_end)
        fork do
          Kernel.at_exit do
            File.delete(command.running_path)
            pids_to_end.each { |p| kill("HUP", p) }
          end

          system("#{command.cmd} > #{command.stdout_path} 2> #{command.stderr_path}")
        end
      end
    end
  end
end
