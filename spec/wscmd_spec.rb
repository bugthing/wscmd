# frozen_string_literal: true

require "tempfile"

RSpec.describe Wscmd do
  it "has a version number" do
    expect(Wscmd::VERSION).not_to be nil
  end

  it "runs a cmd and yields the stdout and stderr" do
    test_file1 = Tempfile.new("out")
    test_file1.close
    test_file2 = Tempfile.new("err")
    test_file2.close

    exit_code = described_class
                .stdout(->(line) { File.write(test_file1.path, "log:#{line}", mode: "a") })
                .stderr(->(line) { File.write(test_file2.path, "err:#{line}", mode: "a") })
                .run("bash -c 'echo out; echo err 1>&2; sleep 0.5'")

    expect(File.read(test_file1.path)).to eq "log:out\n"
    expect(File.read(test_file2.path)).to eq "err:err\n"
    expect(exit_code).to eq 0

    test_file1.unlink
    test_file2.unlink
  end
end
